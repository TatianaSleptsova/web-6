window.addEventListener('DOMContentLoaded', function (event) {
    let cost = {
        flowers: {
            siren: 100,
            liliy: 1000,
            roza: 300,
            tylpan: 400,
        },
        service: {
            gol: 30,
            bag: 50,
        },
        color: [0, 10, 20, 30, 40, 50],
        dost: 600,
    }
    let s = document.getElementsByClassName("flowers");
    let dil = document.getElementById("dil");
    let service1 = document.getElementById("service1");
    let service2 = document.getElementById("service2");
    let radios1 = document.getElementById("radios1");
    let radios2 = document.getElementById("radios2");
    let kol = document.getElementById("kolvo");
    let colorrose = document.getElementsByName("colorrose");
    service1.style.display = "none";
    radios1.style.display = "none";
    radios2.style.display = "none";
    service2.style.display = "none";
    kol.style.display = "none";


    let result = document.getElementById("stoimost");
    let cena = 0;
    let lastCostColor1 = 0;
    var kolvo = 1;
    let re = /\D/;


    s[0].addEventListener("change", function (event) {
        let select = event.target;
        console.log(select.value);
        if (select.value == "liliy") {
            cena = cost.flowers[select.value];
            result.innerHTML = ("Стоимость заказа: " + cena);
            radios2.style.display = "none";
            radios1.style.display = "none";
            service2.style.display = "none";
            service1.style.display = "none";
            kol.style.display = "none";
            dil.style.display = "block";
            lastCostColor1 = 0;
            kolvo = 1;
        }
        if (select.value == "roza") {
            cena = cost.flowers[select.value];
            radios2.style.display = "none";
            radios1.style.display = "block";
            service2.style.display = "none";
            service1.style.display = "none";
            kol.style.display = "block";
            dil.style.display = "block";
            lastCostColor1 = 0;
            kolvo = 1;

        }
        if (select.value == "siren") {
            cena = cost.flowers[select.value];
            // result.innerHTML = ("Стоимость заказа: " + cena);
            radios2.style.display = "block";
            radios1.style.display = "none";
            service2.style.display = "block";
            service1.style.display = "none";
            kol.style.display = "block";
            dil.style.display = "block";
            lastCostColor2 = 0;
            kolvo = 1;

        }
        if (select.value == "tylpan") {
            cena = cost.flowers[select.value];
            //result.innerHTML = ("Стоимость заказа: " + cena);
            radios2.style.display = "none";
            radios1.style.display = "none";
            service2.style.display = "none";
            service1.style.display = "block";
            kol.style.display = "block";
            dil.style.display = "block";
            kolvo = 1;

        }

    });

    service1.addEventListener("change", function (event) {
        let r = event.target;
        cena -= lastCostService1;
        cena += cost.service[r.value];
        lastCostService1 = cost.service[r.value];
        result.innerHTML = ("Стоимость заказа: " + cena);
        if (!r.checked) {
            cena -= lastCostService1;
            result.innerHTML = ("Стоимость заказа: " + cena);
            lastCostService1 = 0;
        }
    });
    let lastCostService2 = 0;
    service2.addEventListener("change", function (event) {
        let r = event.target;
        cena -= lastCostService2;
        cena += cost.service[r.value];
        lastCostService2 = cost.service[r.value];
        result.innerHTML = ("Стоимость заказа: " + cena);
        if (!r.checked) {
            cena -= lastCostService2;
            result.innerHTML = ("Стоимость заказа: " + cena);
            lastCostService2 = 0;
        }
    });
    colorrose.forEach(function (radio) {
        radio.addEventListener("change", function (event) {
            let r = event.target;
            cena = cost.flowers.roza;
        if (kol.value < 0 || kol.value.match(re) != null)
                alert("Вы ввели неверно кол-во веток");
            else {
                cena += cost.color[r.value];
                cena *= kol.value;
                if (dil.checked) {
                    cena += cost.dost;
                }
                result.innerHTML = ("Стоимость заказа: " + cena);
            }

        });
    });

    let colorsir = document.getElementsByName("colorsir");
    colorsir.forEach(function (radio) {
        radio.addEventListener("change", function (event) {
            let r = event.target;
            cena = cost.flowers.siren;
            if (kol.value < 0 || kol.value.match(re) === null)
                alert("Вы ввели неверно кол-во веток");
            else {
                cena += cost.color[r.value];
                cena *= kol.value;
                if (dil.checked) {
                    cena += cost.dost;
                }
                result.innerHTML = ("Стоимость заказа: " + cena);
            }

        });
    });
    let dost = 0;
    dil.addEventListener("change", function (event) {
        let r = event.target;
        cena -= dost;
        cena += cost.dost;
        dost = cost.dost;
        result.innerHTML = ("Стоимость заказа: " + cena);
        if (!r.checked) {
            cena -= cost.dost;
            result.innerHTML = ("Стоимость заказа: " + cena);
            dost = 0;
        }
    });
});
